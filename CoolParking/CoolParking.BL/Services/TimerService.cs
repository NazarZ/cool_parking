﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using CoolParking.BL.Interfaces;
using System;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private Timer _aTimer;
        public double Interval { get; set; }

        public event ElapsedEventHandler Elapsed;

        public void Dispose()
        {
            _aTimer.Dispose();
            GC.SuppressFinalize(this);
            
        }

        public void Start()
        {
            _aTimer = new Timer();
            _aTimer.Interval = Interval * 1000;
            _aTimer.AutoReset = true;
            _aTimer.Elapsed += Elapsed;
            _aTimer.Enabled = true;
            _aTimer.Start();
        }

        public void Stop()
        {
            _aTimer.Enabled = false;
        }
    }
}