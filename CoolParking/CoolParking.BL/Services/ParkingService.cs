﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly ILogService _logService;
        private readonly ITimerService _withdrawTimer;
        private readonly ITimerService _logTimer;
        private readonly Parking _parking;
        private readonly object _lockerLog = new();
        private readonly object _lockerTax = new();

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _parking = Parking.GetParking();
            _withdrawTimer = withdrawTimer;
            _withdrawTimer.Elapsed += TaxVehicle;
            _logTimer = logTimer;
            _logTimer.Elapsed += WriteLog;
            _logService = logService;
            _logTimer.Start();
            _withdrawTimer.Start();
        }

        private void WriteLog(Object source, ElapsedEventArgs e)
        {
            _parking.LastParkingBalance = _parking.ParkingBalance;
            if (_parking.Transaction.Count() > 0)
            {
                lock (_lockerLog)
                {
                    foreach (var i in _parking.Transaction)
                    {
                        _logService.Write($"{i.TransactionTime} {i.VehicleId} {i.VehicleType.ToString()} {i.Sum} VehicleBalance: {i.VehicleBalance}");
                    }
                    _parking.Transaction.Clear();
                }
            }
            else
            {
                _logService.Write($"{DateTime.Now}: no transaction");
            }
        }

        private void TaxVehicle(Object source, ElapsedEventArgs e)
        {
            foreach (var v in _parking.Vehicles)
            {
                decimal tax;
                if (Settings.Tarrifs.TryGetValue(v.VehicleType, out tax))
                {
                    lock (_lockerTax)
                    {
                        if (v.Balance - tax >= 0)
                        {
                            v.Balance -= tax;
                            _parking.ParkingBalance += tax;
                        }
                        else if (v.Balance < tax && v.Balance > 0)
                        {
                            var i = v.Balance + (tax - v.Balance) * Settings.FineRatio;
                            v.Balance -= i;
                            _parking.ParkingBalance += i;
                        }
                        else
                        {
                            var i = tax * Settings.FineRatio;
                            v.Balance -= i;
                            _parking.ParkingBalance += i;
                        }

                        _parking.Transaction.Add(new TransactionInfo()
                        {
                            Sum = tax,
                            VehicleType = v.VehicleType,
                            VehicleId = v.Id,
                            VehicleBalance = v.Balance
                        });
                    }
                }
            }
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (_parking.GetFreePlaces() > 0)
            {
                _parking.Vehicles.Add(vehicle);
            }
            else
            {
                throw new InvalidOperationException("No free places");
            }
        }

        public void Dispose()
        {
            _parking.Transaction.Clear();
            _parking.Vehicles.Clear();
            _parking.ParkingBalance = 0;
            GC.SuppressFinalize(this);
        }

        public decimal GetBalance()
        {
            return _parking.ParkingBalance;
        }

        public decimal GetBalanceBeforeLogs()
        {
            return _parking.ParkingBalance - _parking.LastParkingBalance;
        }

        public int GetCapacity()
        {
            return _parking.GetCapacity();
        }

        public int GetFreePlaces()
        {
            return _parking.GetFreePlaces();
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _parking.Transaction.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(_parking.Vehicles.ToList());
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            _parking.RemoveVehicleById(vehicleId);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum > 0)
                _parking.ToUpBalanceVehicle(vehicleId, sum);
            else
                throw new ArgumentException("Sum must be above a zero");
        }
    }
}