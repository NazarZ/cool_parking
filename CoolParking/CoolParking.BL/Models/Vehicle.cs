﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private static readonly Random _random = new();
        private static readonly Parking _parking = Parking.GetParking();

        [Required]
        public string Id { get; }

        [Required]
        public VehicleType VehicleType { get; }

        [Required]
        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            Id = IsCorrectId(id) ? id : throw new ArgumentException("Vehicle Number is not valid");

            VehicleType = vehicleType;

            Balance = balance > 0 ? balance : throw new ArgumentException("Balance must be above a zero");
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            while (true)
            {
                var newId = RandomCh() + RandomDigit() + RandomCh();
                if (_parking.Vehicles.Where(x => x.Id == newId).Count() == 0)
                    return newId;
            }
        }

        private static string RandomDigit()
        {
            return "-" + new string(Enumerable.Repeat("0123456789", 4)
             .Select(s => s[_random.Next(s.Length)]).ToArray()) + "-";
        }

        private static string RandomCh()
        {
            return new string(Enumerable.Repeat("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 2)
            .Select(s => s[_random.Next(s.Length)]).ToArray());
        }

        private static bool IsCorrectId(string id)
        {
            var buf = id.Split('-');
            if (buf.Length == 3)
            {
                if (Regex.IsMatch(buf[0], "^[A-Z]*$") && Regex.IsMatch(buf[2], "^[A-Z]*$") && Regex.IsMatch(buf[1], "^[0-9]*$"))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            Vehicle otherVehicle = obj as Vehicle;
            if (otherVehicle != null)
            {
                return Id.Equals(otherVehicle.Id);
            }
            else
            {
                throw new ArgumentException("Type of object isn't valid");
            }
        }
    }
}